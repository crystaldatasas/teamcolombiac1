package com.colombia.worldcinema

class Constants {
    companion object{
        var MY_PREFS_NAME = BuildConfig.APPLICATION_ID
        var LOGGED = "logged"
        var BASE_URL = "http://cinema.areas.su"

        // Signup Api POST
        var sign_up: String = BASE_URL + "/auth/register"
        var sign_in: String = BASE_URL + "/auth/login"
        var movie_cover: String = BASE_URL + "/movies/cover"
        var usemovies: String = BASE_URL + "/usermovies"
        var movies: String = BASE_URL + "/movies"
        var get_movie: String = BASE_URL + "/movies/"
        var get_episode: String = BASE_URL + "/episodes/"
        var imageUrlBase: String = BASE_URL + "/up/images/"
        var videoUrlBase: String = BASE_URL + "/up/video/"
        var TOKEN: String = "token"


    }
}