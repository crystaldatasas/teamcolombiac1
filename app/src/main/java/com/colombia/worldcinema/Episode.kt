package com.colombia.worldcinema

data class Episode (
        var episodeId: String?,
        var name: String?,
        var description:String?,
        var moviesId:String?,
        var director:String?,
        var stars:ArrayList<String>?,
        var year:String?,
        var preview:String?,
        var runtime:String?,
        var images:ArrayList<String>?,

        )

