package com.colombia.worldcinema

import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.os.Bundle
import android.widget.MediaController
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity


class EpisodeActivity : AppCompatActivity() {
    lateinit var videoView:VideoView
    lateinit var title: TextView
    lateinit var description:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_episode)

        videoView = findViewById<VideoView>(R.id.videoView)
        title = findViewById<TextView>(R.id.txtTitle)
        description = findViewById<TextView>(R.id.txtDescription)


        when {
            intent.hasExtra("episodePreview") -> {
                Toast.makeText(
                    applicationContext,
                    intent.getStringExtra("episodePreview"),
                    Toast.LENGTH_LONG
                ).show()
                val uri: Uri = Uri.parse(intent.getStringExtra("episodePreview"))
                videoView.setVideoURI(uri)
                setTitle(intent.getStringExtra("episodeTitle"))
                title.text = intent.getStringExtra("episodeTitle")
                description.text = intent.getStringExtra("episodeDescription")
                val ctlr =  MediaController(this@EpisodeActivity);
                ctlr.setMediaPlayer(videoView);
                videoView.setMediaController(ctlr);
                videoView.requestFocus();
                videoView.start()
                videoView.setOnPreparedListener(OnPreparedListener {
                    //use a global variable to get the object
                    it.start()
                })

            }

        }
    }
}