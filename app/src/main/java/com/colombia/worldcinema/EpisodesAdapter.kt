package com.colombia.worldcinema

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class EpisodesAdapter(
    private var episodes: List<Episode> = emptyList()
) : RecyclerView.Adapter<EpisodesAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.image)
        val title: TextView = view.findViewById(R.id.txtTitle)
        val description: TextView = view.findViewById(R.id.txtDescription)
        val year: TextView = view.findViewById(R.id.txtYear)
        var item: LinearLayout = view.findViewById(R.id.item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.item_episode,
                parent,
                false
            )
        )


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val episode = episodes[position]
        holder.title.text = "${episode.name}"
        holder.description.text = "${episode.description}"
        holder.year.text = "${episode.year}"
        if(episode.images!!.size>0){
            Glide.with(holder.image.context)
                .load(Constants.imageUrlBase + episode.images!![0])
                .into(holder.image)
        }else{
            Glide.with(holder.image.context)
                .load(R.drawable.logo)
                .into(holder.image)
        }



        holder.item.setOnClickListener { v ->

           val intent=Intent(v.context, EpisodeActivity::class.java)
            intent.putExtra("episodeId",episode.episodeId)
            intent.putExtra("episodePreview",Constants.videoUrlBase+episode.preview)
            intent.putExtra("episodeTitle",episode.name)
            intent.putExtra("episodeDescription",episode.description)
            v.context.startActivity(intent)
            Toast.makeText(v.context, "Episode #"+episode.episodeId, Toast.LENGTH_SHORT).show()
        }




    }

    override fun getItemCount(): Int {
        return episodes.size
    }



}