package com.colombia.worldcinema

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.irozon.sneaker.Sneaker
import java.util.*

class Helper {
    companion object{
        fun addPref(
            c: Context,
            clave: String?,
            valor: String?
        ) {
            val prefs =
                c.getSharedPreferences(clave, Context.MODE_PRIVATE)
            val editor = prefs.edit()
            editor.putString(clave, valor)
            editor.apply()
            editor.commit()

        }


        fun getPref(
            c: Context,
            clave: String
        ): String? {
            val prefs =
                c.getSharedPreferences(clave, Context.MODE_PRIVATE)
            return prefs.getString(clave, "")
        }

        fun haveNetworkConnection(context: Context): Boolean {
            var haveConnectedWifi = false
            var haveConnectedMobile = false
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.allNetworkInfo
            for (ni in netInfo) {
                if (ni.typeName.equals("WIFI", ignoreCase = true)) {
                    if (ni.isConnected) {
                        haveConnectedWifi = true
                        Log.v("WIFI CONNECTION ", "AVAILABLE")
                    } else {
                        Log.v("WIFI CONNECTION ", "NOT AVAILABLE")
                    }
                }
                if (ni.typeName.equals("MOBILE", ignoreCase = true)) {
                    if (ni.isConnected) {
                        haveConnectedMobile = true
                        //                    Log.v("MOBILE INTERNET CONNECTION ", "AVAILABLE");
                    } else {
//                    Log.v("MOBILE INTERNET CONNECTION ", "NOT AVAILABLE");
                    }
                }
            }
            return haveConnectedWifi || haveConnectedMobile
        }

        fun alertDialogInternetError(context: Context, message: String){
            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setMessage(message)
            alertDialogBuilder.setPositiveButton(
                context.resources.getString(R.string.exit)
            ) { arg0, arg1 -> (context as Activity).finish() }
            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        }

        fun alertDialog(context: Context, title: String, message: String){
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
                .setPositiveButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                        // FIRE ZE MISSILES!
                    })
            // Create the AlertDialog object and return it
            builder.create()
            builder.show()
        }

        fun errorMsg(context: Context, title: String, message: String){
            Sneaker.with(context as Activity) // Activity, Fragment or ViewGroup
                .setTitle(title)
                .setDuration(4000)
                .setCornerRadius(3, 0)
                .autoHide(true)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setMessage(message)
                .sneakError()
        }

        fun successMsg(context: Context, title: String, message: String){
            Sneaker.with(context as Activity) // Activity, Fragment or ViewGroup
                .setTitle(title)
                .setDuration(4000)
                .setCornerRadius(3, 0)
                .autoHide(true)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setMessage(message)
                .sneakSuccess()
        }

        fun sonar(c: Context?, sonido: Int) {
            val mp = MediaPlayer.create(c, sonido)
            mp.setOnPreparedListener{
                it.start()
            }

        }
    }


}