package com.colombia.worldcinema

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kaopiz.kprogresshud.KProgressHUD
import org.json.JSONObject
import java.util.HashMap

class HomeActivity : AppCompatActivity() {

    var fragment_main: Fragment = MainFragment()
    var fragment_favorite: Fragment = FavoriteFragment()
    var fragment_profile: Fragment = ProfileFragment()
    var fragment_movies: Fragment = MoviesFragment()
    var fm: FragmentManager = supportFragmentManager
    var active: Fragment = fragment_main

    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.navigation_home -> {
                        fm.beginTransaction().hide(active).show(fragment_main).commit()
                        active = fragment_main
                        return true
                    }
                    R.id.navigation_profile -> {
                        fm.beginTransaction().hide(active).show(fragment_profile).commit()
                        active = fragment_profile
                        return true
                    }
                    R.id.navigation_movies -> {
                        fm.beginTransaction().hide(active).show(fragment_movies).commit()
                        active = fragment_movies
                        return true
                    }
                    R.id.navigation_favorites -> {
                        fm.beginTransaction().hide(active).show(fragment_favorite).commit()
                        active = fragment_favorite

                        return true
                    }
                }
                return false
            }
        }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        fm.beginTransaction().add(R.id.main_container, fragment_profile, "4").hide(
            fragment_profile
        )
            .commit()
        fm.beginTransaction().add(R.id.main_container, fragment_favorite, "3").hide(
            fragment_favorite
        )
            .commit()
        fm.beginTransaction().add(R.id.main_container, fragment_movies, "2").hide(fragment_movies)
            .commit()
        fm.beginTransaction().add(R.id.main_container, fragment_main, "1").commit()


        val navigation: BottomNavigationView = findViewById(R.id.nav_view)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

/*
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navigation.setupWithNavController(navController)*/
    }
}