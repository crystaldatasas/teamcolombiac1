package com.colombia.worldcinema

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ImagesAdapter(
    private var images: List<Image> = emptyList()
) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.image)
        var item: LinearLayout = view.findViewById(R.id.item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.item_image,
                parent,
                false
            )
        )


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = images[position]
        //holder.image.text = "${spot.id}. ${spot.name}"
        Glide.with(holder.image.context)
            .load(Constants.imageUrlBase +image.image)
            .into(holder.image)

        holder.item.setOnClickListener { v ->

            //val intent=Intent(v.context, MovieDetailsActivity::class.java)
            //intent.putExtra("imageId",image.movieId)
           // v.context.startActivity(intent)
            Toast.makeText(v.context, image.image, Toast.LENGTH_SHORT).show()
        }




    }

    override fun getItemCount(): Int {
        return images.size
    }



}