package com.colombia.worldcinema

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class LaunchScreenActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT:Long = 2000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_screen)
        Toast.makeText(applicationContext,Helper.getPref(this@LaunchScreenActivity, Constants.LOGGED),Toast.LENGTH_LONG).show()

        if (Helper.haveNetworkConnection(this@LaunchScreenActivity)) {
            Handler().postDelayed({
                Toast.makeText(applicationContext,Helper.getPref(this@LaunchScreenActivity, Constants.LOGGED),Toast.LENGTH_LONG).show()

                if (Helper.getPref(this@LaunchScreenActivity, Constants.LOGGED)=="true") {
                    val i = Intent(this@LaunchScreenActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()

                } else {
                    val i = Intent(this@LaunchScreenActivity, RegisterActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }, SPLASH_TIME_OUT)
        } else {
            Toast.makeText(
                this@LaunchScreenActivity,
                resources.getString(R.string.internet_connection),
                Toast.LENGTH_SHORT
            ).show()
            Helper.alertDialogInternetError(this@LaunchScreenActivity,resources.getString(R.string.internet_connection_message))
        }
    }
}