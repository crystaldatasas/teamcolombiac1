package com.colombia.worldcinema

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.lang.Exception
import java.util.HashMap

class LoginActivity : AppCompatActivity() {
    lateinit var email: EditText
    lateinit var pass: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initGUI()
    }

    private fun initGUI() {
        email = findViewById(R.id.txtEmail)
        pass = findViewById(R.id.txtPass)
    }


    fun login(v: View){
        if(validate()){
                if(email.text.toString().isValidEmail() && email.text.toString() == email.text.toString().toLowerCase()) {
                    val p = KProgressHUD.create(this)
                     p.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                     p.setLabel("Please wait")
                     p.setDetailsLabel("Verifing data")
                     p.setCancellable(true)
                     p.setAnimationSpeed(2)
                     p.setDimAmount(0.5f)
                    try {
                        p.show()
                    }catch (e:Exception){

                    }


                    val result = arrayOf("")

                    //inicio login

                    val stringRequest: StringRequest = object : StringRequest(
                        Method.POST, Constants.sign_in,
                        Response.Listener { response ->
                            Log.e("token_reg", response)
                            if(response.contains("token")){


                                val jsonObject = JSONObject(response)

                                val token = jsonObject.getString("token")
                                Helper.addPref(this@LoginActivity, Constants.TOKEN,token)
                                Helper.addPref(this@LoginActivity, Constants.LOGGED,"true")
                                Helper.successMsg(
                                    this@LoginActivity,
                                    "Excellent",
                                    "Logged with token:"+token
                                )
                                //Toast.makeText(applicationContext,token,Toast.LENGTH_LONG).show()
                                val i = Intent(this@LoginActivity, HomeActivity::class.java)
                                startActivity(i)
                                Helper.sonar(applicationContext,R.raw.success)
                                finish()
                            }else {
                                Helper.alertDialog(
                                    this@LoginActivity,
                                    "Authentication Failed",
                                    response
                                )
                                Helper.sonar(applicationContext,R.raw.error)
                            }
                            p.dismiss()
                        },
                        Response.ErrorListener {
                            //displaying the error in toast if occurrs
                            Helper.sonar(applicationContext,R.raw.error)
                            Helper.alertDialog(
                                this@LoginActivity, "Authentication Failed",
                                resources.getString(R.string.ruMessage)
                            )
                            p.dismiss()
                        }) {
                        override fun getParams(): Map<String, String> {
                            val params: MutableMap<String, String> = HashMap()
                            params["email"] = txtEmail.text.toString().trim()
                            params["password"] = txtPass.text.toString().trim()
                            return params
                        }
                    }
                    //creating a request queue
                    //creating a request queue
                    val requestQueue = Volley.newRequestQueue(this@LoginActivity)
                    //adding the string request to request queue
                    //adding the string request to request queue
                    requestQueue.add(stringRequest)

                    //return result[0]
                    //fin login



                }else{
                    Helper.sonar(applicationContext,R.raw.error)
                    email.requestFocus()
                    email.error = "Incorrect format email"
                    Helper.alertDialog(this@LoginActivity, "Verify data", "Incorrect format email")
                    Helper.errorMsg(this@LoginActivity, "Verify data", "Incorrect format email")
                }

        }else{
            Helper.sonar(applicationContext,R.raw.error)
            Helper.alertDialog(this@LoginActivity, "Verify data", "Fill all data form")
            Helper.errorMsg(this@LoginActivity, "Verify data", "Fill all data form")
        }
    }

    fun goRegister(v:View){
        val i = Intent(this@LoginActivity, RegisterActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun validate(): Boolean {
        val r= (email.text.toString().trim().isNotEmpty()
                && pass.text.toString().trim().isNotEmpty())
        return r
    }
    private fun CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(
        this
    ).matches()
}