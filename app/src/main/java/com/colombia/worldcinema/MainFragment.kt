package com.colombia.worldcinema

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class MainFragment : Fragment() {
  lateinit var imgBack:ImageView
  lateinit var imgFr:ImageView
  lateinit var btWatch:Button
  lateinit var movieId:String
  lateinit var recyclerMovies:RecyclerView
  lateinit var moviesAdapter: MoviesAdapter
  private lateinit var moviesArrayList: ArrayList<Movie>


  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
// Inflate the layout for this fragment
    val vista = inflater.inflate(R.layout.fragment_main, container, false)
    imgBack =vista.findViewById<ImageView>(R.id.img1)
    imgFr =vista.findViewById<ImageView>(R.id.img2)
    btWatch =vista.findViewById<Button>(R.id.btWatch)
    recyclerMovies =vista.findViewById<RecyclerView>(R.id.recycler)
    recyclerMovies.setHasFixedSize(true)
    recyclerMovies.layoutManager= LinearLayoutManager(vista.context,LinearLayoutManager.HORIZONTAL ,false)
    moviesArrayList = ArrayList<Movie>()
    btWatch.setOnClickListener{
      val i = Intent(vista.context, MovieDetailsActivity::class.java)
      i.putExtra("movieId", movieId)
      startActivity(i)
    }

    //Toast.makeText(vista.context, Helper.getPref(vista.context,Constants.TOKEN), Toast.LENGTH_LONG).show()

    loadMain(vista)
    loadMovies(vista)


    return vista;



    // TextView x=(TextView)vista.findViewById(R.id.x);

    // x.setText("¡Hola!");

  }

  private fun loadMain(vista: View) {
    val p = KProgressHUD.create(vista.context)
      .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
      .setLabel("Please wait")
      .setDetailsLabel("Loading...")
      .setCancellable(true)
      .setAnimationSpeed(2)
      .setDimAmount(0.5f)
    p.show()

    val result = arrayOf("")
    val stringRequest: StringRequest = object : StringRequest(
      Method.GET, Constants.movie_cover,
      Response.Listener { response ->
        Log.e("token_reg", response)
        if (response.contains("movieId")) {


          val jsonObject = JSONObject(response)

          movieId = jsonObject.getString("movieId")
          val backgroundImage = Constants.imageUrlBase + jsonObject.getString("backgroundImage")
          val foregroundImage = Constants.imageUrlBase + jsonObject.getString("foregroundImage")
          //Toast.makeText(vista.context,backgroundImage,Toast.LENGTH_LONG).show()
          Glide.with(vista.context)
            .load(backgroundImage)
            .into(imgBack)

          Glide.with(vista.context)
            .load(foregroundImage)
            .into(imgFr)
          p.dismiss()
        } else {
          Toast.makeText(vista.context, response, Toast.LENGTH_LONG).show()
          p.dismiss()
        }

      },
      Response.ErrorListener {
        //displaying the error in toast if occurrs

        p.dismiss()
      }) {
      override fun getParams(): Map<String, String> {
        val params: MutableMap<String, String> = HashMap()

        return params
      }
    }
    //creating a request queue
    //creating a request queue
    val requestQueue = Volley.newRequestQueue(vista.context)
    //adding the string request to request queue
    //adding the string request to request queue
    requestQueue.add(stringRequest)

    //return result[0]
    //fin login

  }

  private fun loadMovies(vista: View) {
    val movies: ArrayList<Movie>? = ArrayList()
    val p = KProgressHUD.create(vista.context)
      .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
      .setLabel("Please wait")
      .setDetailsLabel("Loading...")
      .setCancellable(true)
      .setAnimationSpeed(2)
      .setDimAmount(0.5f)
    p.show()

    val result = arrayOf("")
    val stringRequest: StringRequest = object : StringRequest(
      Method.GET, Constants.movies,
      Response.Listener { response ->
        Log.e("token_reg", response)
        if (response.contains("movieId")) {
          try {

            val jsonArray = JSONArray(response)
            for (i in 0 until jsonArray.length()) {
              val jsonObj = jsonArray.getJSONObject(i)
              val movie = Movie(
                jsonObj.getString("movieId"),
                jsonObj.getString("poster"),
                jsonObj.getString("age")
              )

              moviesArrayList!!.add(movie)
             /* moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)
              moviesArrayList!!.add(movie)*/


            }
            moviesAdapter = MoviesAdapter(moviesArrayList)
            recyclerMovies.adapter = moviesAdapter
            p.dismiss()
          } catch (e: JSONException) {
            e.printStackTrace()
          }


        } else {
          Toast.makeText(vista.context, response, Toast.LENGTH_LONG).show()
          p.dismiss()
        }

      },
      Response.ErrorListener {
        //displaying the error in toast if occurrs

        p.dismiss()
      }) {
      override fun getParams(): Map<String, String> {
        val params: MutableMap<String, String> = HashMap()

        return params
      }
    }
    //creating a request queue
    //creating a request queue
    val requestQueue = Volley.newRequestQueue(vista.context)
    //adding the string request to request queue
    //adding the string request to request queue
    requestQueue.add(stringRequest)

    //return result[0]
    //fin login

  }
}