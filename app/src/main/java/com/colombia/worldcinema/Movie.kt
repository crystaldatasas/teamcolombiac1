package com.colombia.worldcinema

data class Movie (
        var movieId: String?,
        var poster: String?,
        var age:String?
    )

