package com.colombia.worldcinema

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.lujun.androidtagview.TagContainerLayout
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class MovieDetailsActivity : AppCompatActivity() {
    lateinit var imgBack: ImageView
    lateinit var imgFr: ImageView
    lateinit var btWatch: Button
    lateinit var movieId:String
    lateinit var rvTags:RecyclerView
    lateinit var txtAge: TextView
    lateinit var txtDescription: TextView
    private lateinit var tagArrayList: ArrayList<String>

    lateinit var recyclerImage:RecyclerView
    lateinit var imageAdapter: ImagesAdapter
    private lateinit var imageArrayList: ArrayList<Image>

    lateinit var recyclerEpisodes:RecyclerView
    lateinit var episodesAdapter: EpisodesAdapter
    private lateinit var episodesArrayList: ArrayList<Episode>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        loadGUI()
        loadMovie()
        loadMovieEpisode("2")

    }

    override fun onDestroy() {
        super.onDestroy()
        Helper.addPref(applicationContext, "movieId", "")
    }

    private fun loadGUI() {
        Toast.makeText(
            applicationContext,
            Helper.getPref(applicationContext, Constants.TOKEN).toString(),
            Toast.LENGTH_LONG
        ).show()
        when {
            intent.hasExtra("movieId") -> {
                movieId = intent.getStringExtra("movieId")
                Helper.addPref(applicationContext, "movieId", movieId)
            }
            Helper.getPref(applicationContext, "movieId").toString().isNotEmpty() -> {
                movieId = Helper.getPref(applicationContext, "movieId").toString()
                Helper.addPref(applicationContext, "movieId", movieId)
            }
            else -> {
                movieId = "1"//movieId with tags and description for test
            }
        }
        movieId = "2"//movieId with tags and description for test
        movieId = "1"//movieId with tags and description for test
        imgBack =findViewById<ImageView>(R.id.img1)
        imgFr =findViewById<ImageView>(R.id.img2)
        txtAge =findViewById<TextView>(R.id.txtAge)
        txtDescription =findViewById<TextView>(R.id.txtDescription)
        rvTags =findViewById<RecyclerView>(R.id.rvTags)
        btWatch =findViewById<Button>(R.id.btWatch)
        btWatch.setOnClickListener{
            //val i = Intent(applicationContext, MovieDetailsActivity::class.java)
           // i.putExtra("movieId",movieId)
           // startActivity(i)
        }

        recyclerImage =findViewById<RecyclerView>(R.id.recyclerScreen)
        recyclerImage.setHasFixedSize(true)
        recyclerImage.layoutManager= LinearLayoutManager(
            applicationContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        imageArrayList = ArrayList<Image>()

        recyclerEpisodes =findViewById<RecyclerView>(R.id.recyclerEpisodes)
        recyclerEpisodes.setHasFixedSize(true)
        recyclerEpisodes.layoutManager= LinearLayoutManager(
            applicationContext,
            LinearLayoutManager.VERTICAL,
            false
        )
        episodesArrayList = ArrayList<Episode>()
    }

    private fun loadMovie() {
        val p = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setDetailsLabel("Loading...")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
        p.show()
        tagArrayList = ArrayList<String>()
        val result = arrayOf("")
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET, Constants.get_movie + movieId,
            Response.Listener { response ->
                Log.e("token_reg", response)
                if (response.contains("movieId")) {


                    val jsonObject = JSONObject(response)
                    title = jsonObject.getString("name")
                    movieId = jsonObject.getString("movieId")
                    val backgroundImage = Constants.imageUrlBase + jsonObject.getString("poster")
                    val age = jsonObject.getString("age").toInt()
                    if (age >= 16) {
                        txtAge.setTextColor(resources.getColor(R.color.colorAccent))
                    } else {
                        txtAge.setTextColor(resources.getColor(R.color.colorGreen))
                    }
                    txtAge.text = "${age}+"
                    txtDescription.text = jsonObject.getString("description")
                    // Toast.makeText(applicationContext,jsonObject.getString("tags"),Toast.LENGTH_LONG).show()
                    Glide.with(applicationContext)
                        .load(backgroundImage)
                        .into(imgBack)

                    //Tags
                    val jsonArray = JSONArray(jsonObject.getString("tags"))
                    for (i in 0 until jsonArray.length()) {
                        val jsonObj = jsonArray.getJSONObject(i)
                        val t = Tag(
                            jsonObj.getString("idTags"),
                            jsonObj.getString("tagName")
                        )

                        tagArrayList!!.add(t.name.toString())
                    }
                    val mTagContainerLayout =
                        findViewById<View>(R.id.tagcontainerLayout) as TagContainerLayout
                    mTagContainerLayout.tags = tagArrayList

                    //Images

                    val jsonArrayImages = JSONArray(jsonObject.getString("images"))
                    for (i in 0 until jsonArrayImages.length()) {
                        val i = Image(
                            "",
                            jsonArrayImages.getString(i)
                        )

                        imageArrayList!!.add(i)
                    }
                    imageAdapter = ImagesAdapter(imageArrayList)
                    recyclerImage.adapter = imageAdapter

                    loadMovieEpisodes(movieId)
                    p.dismiss()
                } else {
                    Toast.makeText(applicationContext, response, Toast.LENGTH_LONG).show()
                    p.dismiss()
                }

            },
            Response.ErrorListener {
                //displaying the error in toast if occurrs

                p.dismiss()
            }) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()

                return params
            }
        }
        //creating a request queue
        //creating a request queue
        val requestQueue = Volley.newRequestQueue(this)
        //adding the string request to request queue
        //adding the string request to request queue
        requestQueue.add(stringRequest)

        //return result[0]
        //fin login

    }

    private fun loadMovieEpisodes(movieId: String) {
        val p = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setDetailsLabel("Loading Episodes...")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
        p.show()
        episodesArrayList = ArrayList<Episode>()
        val result = arrayOf("")
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET, Constants.get_movie + movieId + "/episodes",
            Response.Listener { response ->
                Log.e("token_reg", response)
                val jsonArrayEpisodes = JSONArray(response)
                if (jsonArrayEpisodes.length() > 0) {

                    //Episodes

                    for (i in 0 until jsonArrayEpisodes.length()) {
                        val jsonObj = jsonArrayEpisodes.getJSONObject(i)
                        val jsonArrayStars = JSONArray(jsonObj.getString("stars"))
                        val jsonArrayImages = JSONArray(jsonObj.getString("images"))

                        var stars = ArrayList<String>()
                        for (i2 in 0 until jsonArrayStars.length()) {
                            stars.add(jsonArrayStars.getString(i2))
                        }

                        var images = ArrayList<String>()
                        for (i3 in 0 until jsonArrayImages.length()) {
                            images.add(jsonArrayImages.getString(i3))
                        }

                        val episode = Episode(
                            jsonObj.getString("episodeId"),
                            jsonObj.getString("name"),
                            jsonObj.getString("description"),
                            jsonObj.getString("moviesId"),
                            jsonObj.getString("director"),
                            stars,
                            jsonObj.getString("year"),
                            jsonObj.getString("preview"),
                            jsonObj.getString("runtime"),
                            images
                        )

                        episodesArrayList!!.add(episode)
                    }

                    episodesAdapter = EpisodesAdapter(episodesArrayList)
                    recyclerEpisodes.adapter = episodesAdapter

                    p.dismiss()
                } else {
                    Toast.makeText(applicationContext, response, Toast.LENGTH_LONG).show()
                    p.dismiss()
                }

            },
            Response.ErrorListener {
                //displaying the error in toast if occurrs

                p.dismiss()
            }) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()

                return params
            }

            override fun getBodyContentType(): String? {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>? {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "2e96e0a4ff05ba86dc8f778ac49a8dc0"
                headers["Content-Type"] = "application/json; charset=UTF-8"
                headers["token"] = Helper.getPref(applicationContext, Constants.TOKEN)!!
                return headers
            }

        }
        //creating a request queue
        //creating a request queue
        val requestQueue = Volley.newRequestQueue(this)
        //adding the string request to request queue
        //adding the string request to request queue
        requestQueue.add(stringRequest)

        //return result[0]
        //fin login
    }

    private fun loadMovieEpisode(episodeId: String) {
        val p = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setDetailsLabel("Loading Episode...")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
        p.show()
val url = Constants.get_episode +episodeId+"/time"
        val stringRequest: StringRequest = object : StringRequest(
            Method.POST, url,
            Response.Listener { response ->
                Log.e("Epi token_reg", response)
                Toast.makeText(applicationContext, "OKIII "+response, Toast.LENGTH_LONG).show()


                    p.dismiss()


            },
            Response.ErrorListener {
                //displaying the error in toast if occurrs
                p.dismiss()
            }) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["episodeId"] = episodeId
                return params
            }

            override fun getBodyContentType(): String? {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>? {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Bearer 1147026"
                headers["Content-Type"] = "application/json; charset=UTF-8"
                headers["token"] = Helper.getPref(this@MovieDetailsActivity, Constants.TOKEN)!!
                return headers
            }

        }
        //creating a request queue
        //creating a request queue
        val requestQueue = Volley.newRequestQueue(this)
        //adding the string request to request queue
        //adding the string request to request queue
        requestQueue.add(stringRequest)

        //return result[0]
        //fin login
    }
}