package com.colombia.worldcinema;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista=inflater.inflate(R.layout.fragment_profile, container, false);
        // TextView x=(TextView)vista.findViewById(R.id.x);

        // x.setText("¡Hola!");
        Helper.Companion.addPref(vista.getContext(),Constants.Companion.getTOKEN(),"");
        Helper.Companion.addPref(vista.getContext(), Constants.Companion.getLOGGED(),"");
        vista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.Companion.sonar(v.getContext(),R.raw.success);
                startActivity(new Intent(vista.getContext(), LoginActivity.class));
                Activity ac=(Activity)vista.getContext();
                ac.finish();
            }
        });




        return vista;
    }

}
