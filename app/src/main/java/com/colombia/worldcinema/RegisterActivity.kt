package com.colombia.worldcinema

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.util.*

class RegisterActivity : AppCompatActivity() {
    lateinit var firstName:EditText
    lateinit var lastName:EditText
    lateinit var email:EditText
    lateinit var pass:EditText
    lateinit var repass:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initGUI()
    }

    private fun initGUI() {
        firstName = findViewById(R.id.txtFirstName)
        lastName = findViewById(R.id.txtLastName)
        email = findViewById(R.id.txtEmail)
        pass = findViewById(R.id.txtPass)
        repass = findViewById(R.id.txtPass2)
    }

    fun register(v: View){
        if(validate()){
            if(pass.text.toString() == repass.text.toString()){
                if(email.text.toString().isValidEmail() && email.text.toString() == email.text.toString().toLowerCase()) {
                    val p = KProgressHUD.create(this@RegisterActivity)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Please wait")
                        .setDetailsLabel("Saving information")
                        .setCancellable(true)
                        .setAnimationSpeed(2)
                        .setDimAmount(0.5f)
                        p.show()

                        val result = arrayOf("")

                        val stringRequest: StringRequest = object : StringRequest(
                            Method.POST, Constants.sign_up,
                            Response.Listener { response ->
                                Log.e("token_reg", response)
                                if (response == "Успешная регистрация") {
                                    Helper.successMsg(this@RegisterActivity, "OKI", "Registrado")
                                    //inicio login

                                    val stringRequest: StringRequest = object : StringRequest(
                                        Method.POST, Constants.sign_in,
                                        Response.Listener { response ->
                                            Log.e("token_reg", response)
                                            if(response.contains("token")){


                                            val jsonObject = JSONObject(response)

                                            val token = jsonObject.getString("token")
                                                Helper.addPref(applicationContext,Constants.TOKEN,token)
                                                Helper.addPref(applicationContext, Constants.LOGGED,"true")
                                                Helper.successMsg(
                                                    this@RegisterActivity,
                                                    "Excellent",
                                                    "Logged with token:"+token
                                                )
                                                val i = Intent(this@RegisterActivity, HomeActivity::class.java)
                                                startActivity(i)
                                                Helper.sonar(applicationContext,R.raw.success)
                                                finish()
                                            }else {
                                                Helper.alertDialog(
                                                    this@RegisterActivity,
                                                    "Nooooo",
                                                    "Revise"
                                                )
                                            }
                                            p.dismiss()
                                        },
                                        Response.ErrorListener {
                                            //displaying the error in toast if occurrs
                                            Helper.errorMsg(
                                                this@RegisterActivity, "Error Volley",
                                                it.message.toString()
                                            )
                                            Helper.sonar(applicationContext,R.raw.error)
                                            p.dismiss()
                                        }) {
                                        override fun getParams(): Map<String, String> {
                                            val params: MutableMap<String, String> = HashMap()
                                            params["email"] = txtEmail.text.toString().trim()
                                            params["password"] = txtPass.text.toString().trim()
                                            return params
                                        }
                                    }
                                    //creating a request queue
                                    //creating a request queue
                                    val requestQueue = Volley.newRequestQueue(this@RegisterActivity)
                                    //adding the string request to request queue
                                    //adding the string request to request queue
                                    requestQueue.add(stringRequest)

                                    //return result[0]
                                    //fin login
                                } else {
                                    Helper.sonar(applicationContext,R.raw.error)
                                    Helper.errorMsg(this@RegisterActivity, "Nooooo", "Revise")
                                    Helper.alertDialog(this@RegisterActivity, "Nooooo", "Revise")
                                }
                                p.dismiss()
                            },
                            Response.ErrorListener {
                                Helper.sonar(applicationContext,R.raw.error)
                                //displaying the error in toast if occurrs
                                Helper.errorMsg(
                                    this@RegisterActivity, "Error Volley",
                                    it.message.toString()
                                )
                                p.dismiss()
                            }) {
                            override fun getParams(): Map<String, String> {
                                val params: MutableMap<String, String> = HashMap()
                                params["email"] = txtEmail.text.toString().trim()
                                params["password"] = txtPass.text.toString().trim()
                                params["firstName"] = txtFirstName.text.toString().trim()
                                params["lastName"] = txtLastName.text.toString().trim()
                                return params
                            }
                        }
                        //creating a request queue
                        //creating a request queue
                        val requestQueue = Volley.newRequestQueue(this@RegisterActivity)
                        //adding the string request to request queue
                        //adding the string request to request queue
                        requestQueue.add(stringRequest)



                }else{
                    Helper.sonar(applicationContext,R.raw.error)
                    email.requestFocus()
                    email.error = "Incorrect format email"
                    Helper.errorMsg(this@RegisterActivity, "Verify data", "Incorrect format email")
                    Helper.alertDialog(this@RegisterActivity, "Verify data", "Incorrect format email")
                }
            }else{
                Helper.sonar(applicationContext,R.raw.error)
                pass.text.clear()
                repass.text.clear()
                pass.requestFocus()
                pass.error = "Passwords are differents"
                Helper.errorMsg(this@RegisterActivity, "Verify data", "Passwords are differents")
                Helper.alertDialog(this@RegisterActivity, "Verify data", "Passwords are differents")
            }
        }else{
            Helper.sonar(applicationContext,R.raw.error)
            Helper.errorMsg(this@RegisterActivity, "Verify data", "Fill all data form")
            Helper.alertDialog(this@RegisterActivity, "Verify data", "Fill all data form")
        }
    }

    private fun validate(): Boolean {
        val r= (firstName.text.toString().trim().isNotEmpty()
                && lastName.text.toString().trim().isNotEmpty()
                && email.text.toString().trim().isNotEmpty()
                && pass.text.toString().trim().isNotEmpty()
                && repass.text.toString().trim().isNotEmpty())
        return r
    }
    private fun CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(
        this
    ).matches()

    fun goLogin(v:View){
        val i = Intent(this@RegisterActivity, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

}